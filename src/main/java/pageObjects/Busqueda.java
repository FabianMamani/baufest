package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class Busqueda {
	static int _cantidadPorPag = 6;
	static JavascriptExecutor scroll ;
	static By _btnNextPage = By.id("next2");
	static By _btnAddCart = By.xpath("/html/body/div[5]/div/div[2]/div[2]/div/a");
	static By _cart = By.id("cartur");
	public static void buscarPor(String _opcion, WebDriver driver) throws InterruptedException
	{
		scroll = (JavascriptExecutor) driver;
	    scroll.executeScript("javascript:window.scrollBy(250,350)");
	    
		int posicion=2;
		String fila;
		
		boolean estado= false;
		while (estado!= true)
		{
			fila = "/html/body/div[5]/div/div[1]/div/a["+posicion+"]";
			System.out.println(driver.findElement(By.xpath(fila)).getText());
			if (driver.findElement(By.xpath(fila)).getText().equals(_opcion))
			{
				driver.findElement(By.xpath(fila)).click();
				 estado = true;
			}
			posicion++;
		}
		Thread.sleep(3000);
		
	}
	
	public static void elegir(String obj, WebDriver driver) throws InterruptedException
	{
		Thread.sleep(1000);
		int indiceElemento = 1;
		String path;
				
		boolean encontrado= false;
		while (encontrado != true) 
		{
			path = "/html/body/div[5]/div/div[2]/div/div["+indiceElemento+"]/div/div/h4/a";
			System.out.println(driver.findElement(By.xpath(path)).getText());
			if (driver.findElement(By.xpath(path)).getText().equals(obj)) 
			{
				driver.findElement(By.xpath(path)).click();
				encontrado = true;
			}
			if (indiceElemento == _cantidadPorPag ) 
			{
				driver.findElement(_btnNextPage).click();
				indiceElemento = 1;
				continue;
			}
			
			indiceElemento ++;
		}
		
	}
	
	public static void agregarAlCarrito(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElement(_btnAddCart).click();
	}
	
	public static boolean verificarCart(WebDriver driver, String obj) throws InterruptedException
	{
		int posicion = 1;
		String objCart;
		driver.findElement(_cart).click();
		Thread.sleep(2000);
		boolean encontrado = false;
		while (encontrado!= true)
		{
			objCart= "/html/body/div[6]/div/div[1]/div/table/tbody/tr["+posicion+"]/td[2]";
			if(driver.findElement(By.xpath(objCart)).getText().equals(obj))
			{
				encontrado = true;
			}
			
			posicion++;
		}
		
		return encontrado;
	}

	
}

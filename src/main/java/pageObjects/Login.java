package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Login {
	static By _userBox = By.id("loginusername");
    static By _passwordBox = By.id("loginpassword");
    static By _btnLogin = By.xpath("/html/body/div[3]/div/div/div[3]/button[2]");
    static By _btnWelcome = By.id("nameofuser");
    public static void login(String user, String pass, WebDriver driver) throws InterruptedException {
    	Thread.sleep(3000);
    	//driver.findElement(_userBox).click();
    	driver.findElement(_userBox).sendKeys(user);
    	driver.findElement(_passwordBox).sendKeys(pass);
    	driver.findElement(_btnLogin).click();
    }
    
    public static  boolean statusLogin(String user, WebDriver driver) throws InterruptedException {
    	Thread.sleep(3000);
    	System.out.println(driver.findElement(_btnWelcome).getText());
    	return driver.findElement(_btnWelcome).getText().equals("Welcome "+user);
    }

}

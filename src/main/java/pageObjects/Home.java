package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public abstract class Home {
	
	private static By _btnSingUp = By.id("signin2");
    private static By _btnLogin = By.id("login2");
	
	
	public static void visitarPaginaPpal(WebDriver driver)
	{
		driver.manage().window().maximize();
		driver.get("https://demoblaze.com/index.html");
		
		
	}
	
	public static void irMenuRegistro(WebDriver driver)
	{
		driver.findElement(_btnSingUp).click();
	}
	
	public static void irMenuLogin(WebDriver driver)
	{
		driver.findElement(_btnLogin).click();
	}
	
	public static boolean checkVisible(WebDriver driver, String id)
	{
		return driver.findElement(By.id(id)).isDisplayed();
	}
	
}

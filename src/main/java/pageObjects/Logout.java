package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Logout {
	static By _btnLogout = By.id("logout2");
	
	public static void  Logout(WebDriver driver) throws InterruptedException
	{
		Thread.sleep(3000);
		driver.findElement(_btnLogout).click();
	}
	
	public static void finalizarDriver(WebDriver driver)
	{
		driver.quit();
	}
}

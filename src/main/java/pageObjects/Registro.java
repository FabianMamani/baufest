package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class Registro {
	static By _userBox = By.id("sign-username");
    static By _passwordBox = By.id("sign-password");
    static By _btnSignUp = By.xpath("/html/body/div[2]/div/div/div[3]/button[2]");
    
	
	public static void registrar(String usuario, String pass,WebDriver driver) throws InterruptedException 
	{
		Thread.sleep(2000);
		driver.findElement(_userBox).click();
		driver.findElement(_userBox).sendKeys(usuario);
		driver.findElement(_passwordBox).sendKeys(pass);
		driver.findElement(_btnSignUp).click();
		Thread.sleep(3000);
		
	}
		
	public boolean registroStatus() {
		
		return true;
	}

}

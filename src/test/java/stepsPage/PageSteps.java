package stepsPage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Random;

import javax.security.auth.login.LoginContext;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.Busqueda;
import pageObjects.Home;
import pageObjects.Login;
import pageObjects.Logout;
import pageObjects.Registro;

public class PageSteps {
	static WebDriver _driver;
	static String _usuario;
	static String _password;
	static String _obj;
	Random random = new Random();
	int valorDado = random.nextInt(1000);
	
	@Given("^ingreso a la pagina web para registrarme$")
	public void ingreso_a_la_pagina_web_para_registrarme() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
	    _driver=new ChromeDriver();
	    Home.visitarPaginaPpal(_driver);
	}
	
	@When("^si el boton registro esta disponible$")
	public void ingrego_al_menu_de_registro() throws Throwable {
	    // Verifico si el registro esta disponible
	    assertTrue(Home.checkVisible(_driver, "signin2"));
	}

	@Then("^ingreso al menu de registro$")
	public void se_muestra_el_formulario_de_registro() throws Throwable {
	   //Ingreso al menu registro
		Home.irMenuRegistro(_driver);
	}
	
	@Given("^quiero registrarme en la pagina$")
	public void quiero_registrarme_en_la_pagina() throws Throwable {
	   //verifico si el label esta visible
		Thread.sleep(3000);
	   assertTrue(Home.checkVisible(_driver, "signInModalLabel"));
	}
	

	@When("^ingreso <usuario> y <password>$")
	public void ingreso_usuario_y_password(DataTable data) throws Throwable {
		List<String> list = data.asList(String.class);
		_usuario=list.get(0)+valorDado;
		_password= list.get(1);
		Registro.registrar(list.get(0)+valorDado, list.get(1), _driver);
		
	}

	@Then("^el sistema registra mis datos$")
	public void el_sistema_registra_mis_datos() throws Throwable {
	    
		Alert alert = _driver.switchTo().alert();
		assertEquals("Sign up successful.",alert.getText() );
		alert.accept();
	}
	

	@Given("^ingreso al menu de login$")
	public void ingreso_al_menu_de_login() throws Throwable {

	    Home.irMenuLogin(_driver);
	}
	
	@When("^ingreso el usuario u password$")
	public void ingreso_el_usuario_u_password() throws Throwable {
	    
	    Login.login(_usuario, _password, _driver);
	}
	
	@Then("^tengo un logueo exitoso\\.$")
	public void tengo_un_logueo_exitoso() throws Throwable {
	    
	    assertTrue(Login.statusLogin(_usuario, _driver));
	}
	
	@Given("^ingreso al menu y selecciono una <categoria> de busqueda$")
	public void ingreso_al_menu_y_selecciono_una_categoria_de_busqueda(DataTable categoria) throws Throwable {
	    
		List<String> list = categoria.asList(String.class);
	    Busqueda.buscarPor(list.get(0), _driver);
	}

	@When("^selecciono un <objeto>$")
	public void selecciono_un_objeto(DataTable objs) throws Throwable {
	    List<String> pageObjs = objs.asList(String.class);
	    _obj = pageObjs.get(0);
	    Busqueda.elegir(pageObjs.get(0), _driver);
	}

	@Then("^lo agrego al carrito$")
	public void lo_agrego_al_carrito() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   Busqueda.agregarAlCarrito(_driver);
	   Thread.sleep(2000);
	   Alert alertCart = _driver.switchTo().alert();
		assertEquals("Product added.",alertCart.getText() );
		alertCart.accept();
	}

	@Then("^verifico que se agrego correctamente$")
	public void verifico_que_se_agrego_correctamente() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		assertTrue(Busqueda.verificarCart(_driver, _obj));
		
	}
	
	
	@Given("^ingreso al menu logout$")
	public void ingreso_al_menu_logout() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(2000);
		Logout.Logout(_driver);
	}
	
	@Then("^me deslogueo de la cuenta$")
	public void me_deslogueo_de_la_cuenta() throws Throwable {
	    // Verifico si el boton login esta disponible
	    Home.checkVisible(_driver, "login2");
	}
	
	@Then("^finalizo el driver$")
	public void finalizo_el_driver() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    Logout.finalizarDriver(_driver);
	}


}

# Baufest
**Ejercicio Web**


_Precondiciones: Navegador Chrome Versión 86.0.4240.183_

Para llevar a cabo este ejercicio utilizamos las siguientes tecnologias:

- Eclipse
- Selenium WebDriver
- Cucumber
- Junit
- Git
- Maven

_Features_

Se crea un archivo Feature con la finalidad de seleccionar una laptop dentro de la web DemoBlaze. Dicho archivo esta compuesto por 
4 escenarios (Ingreso a la Web, Registro de un usuario, Login del usuario creado y Búsqueda del producto dentro de la web) y sus respectivas definiciones para cada paso de la ejecución del Script.

_Clase Runner_


Contamos con una clase Runner a la cual se le indicará que ejecutaremos los Tests con Cucumber informandole el directorio del archivo Feature, tambien se le agregó el format para el reporte Web.

_Clase PageSteps_

Se crea la implementación de los steps del feature, indicando el código para cada uno de sus pasos definidos.

_Clases Adicionales_


También contamos con clases adicionales con el fin de poder separar el código dependiendo del paso a ejecutar.
Dicha clases son:
- Home
- Registro
- Login
- Busqueda

_Ejecución_

Para la ejecución del proyecto principalmente se deberá contemplar que la versión del navegador Chrome sea compatible con la versión mencionada como precondición. Seguidamente se prosigue con la ejecución del archivo Runner.Java como Junit Test.
Terminada la ejecución, en la carpeta target/Destination se crean los archivos del reporte, en caso de querer visualizarlo se deberá abrir el archivo index.html desde el mismo eclipse con la opción "Web Browser".

**_NOTA:_** Para el caso del usuario, para evitar registrar un usuario ya creado se procedió a la concatenación de la palabra "usuario" y una secuencia de hasta 4 dígitos elegidos aleatoriamente por un Random.


**Web Service**

Para el ejercicio de Web Service se decidió por utilizar la herramienta Postman para la ejecución de las respectivas request.

_Get_

Para la generación del GET se utilizó el siguiente url : https://petstore.swagger.io/v2/pet/3 para poder obtener la mascota identificada con el id = 3.

_Post_

Para el POST primeramente copiaremos el response obtenido en el GET y se tendra el cuenta el formato para el ingreso de una nueva mascota. Se utilizará la URL : https://petstore.swagger.io/v2/pet 

_Put_

Para la ejecución del PUT se copiara el response enviado con el método POST y se le modificará el nombre de la mascota.

Cada uno de estos métodos tienen implementados los scripts para la verificación del status, tiempo de respuesta y Schema del response.
Adjuntamos el link del Repositorio que nos provee Postman:

https://www.getpostman.com/collections/91e3ff7d4ea603211df1


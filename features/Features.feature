#Author: Mamani fabian

Feature: Seleccionar una laptop dentro de la pagina web
  


  Scenario: Ingreso a la web
    Given ingreso a la pagina web para registrarme
    When si el boton registro esta disponible    
    Then ingreso al menu de registro
    


  Scenario: Registrar usuario
    Given quiero registrarme en la pagina
    When ingreso <usuario> y <password> 
    | usuario |     pass1 |
    Then el sistema registra mis datos

 Scenario: Login
 		Given ingreso al menu de login
 		When  ingreso el usuario u password
 		Then tengo un logueo exitoso.
 		
 Scenario: Buscar laptop y agregarla al carrito
 	Given ingreso al menu y selecciono una <categoria> de busqueda
 	|Laptops|
 	When selecciono un <objeto>
 	|Dell i7 8gb|
 	Then lo agrego al carrito
 	And verifico que se agrego correctamente
 
 Scenario: Logout
 	Given ingreso al menu logout
 	Then me deslogueo de la cuenta
 	And finalizo el driver
 	
 		
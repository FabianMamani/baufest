$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Features.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: Mamani fabian"
    }
  ],
  "line": 3,
  "name": "Seleccionar una laptop dentro de la pagina web",
  "description": "",
  "id": "seleccionar-una-laptop-dentro-de-la-pagina-web",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 7,
  "name": "Ingreso a la web",
  "description": "",
  "id": "seleccionar-una-laptop-dentro-de-la-pagina-web;ingreso-a-la-web",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "ingreso a la pagina web para registrarme",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "si el boton registro esta disponible",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "ingreso al menu de registro",
  "keyword": "Then "
});
formatter.match({
  "location": "PageSteps.ingreso_a_la_pagina_web_para_registrarme()"
});
formatter.result({
  "duration": 11261432400,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.ingrego_al_menu_de_registro()"
});
formatter.result({
  "duration": 342879800,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.se_muestra_el_formulario_de_registro()"
});
formatter.result({
  "duration": 239186500,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Registrar usuario",
  "description": "",
  "id": "seleccionar-una-laptop-dentro-de-la-pagina-web;registrar-usuario",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 15,
  "name": "quiero registrarme en la pagina",
  "keyword": "Given "
});
formatter.step({
  "line": 16,
  "name": "ingreso \u003cusuario\u003e y \u003cpassword\u003e",
  "rows": [
    {
      "cells": [
        "usuario",
        "pass1"
      ],
      "line": 17
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "el sistema registra mis datos",
  "keyword": "Then "
});
formatter.match({
  "location": "PageSteps.quiero_registrarme_en_la_pagina()"
});
formatter.result({
  "duration": 3097298700,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.ingreso_usuario_y_password(DataTable)"
});
formatter.result({
  "duration": 5710479000,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.el_sistema_registra_mis_datos()"
});
formatter.result({
  "duration": 30247700,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Login",
  "description": "",
  "id": "seleccionar-una-laptop-dentro-de-la-pagina-web;login",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 21,
  "name": "ingreso al menu de login",
  "keyword": "Given "
});
formatter.step({
  "line": 22,
  "name": "ingreso el usuario u password",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "tengo un logueo exitoso.",
  "keyword": "Then "
});
formatter.match({
  "location": "PageSteps.ingreso_al_menu_de_login()"
});
formatter.result({
  "duration": 222640000,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.ingreso_el_usuario_u_password()"
});
formatter.result({
  "duration": 3429772300,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.tengo_un_logueo_exitoso()"
});
formatter.result({
  "duration": 3098335800,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Buscar laptop y agregarla al carrito",
  "description": "",
  "id": "seleccionar-una-laptop-dentro-de-la-pagina-web;buscar-laptop-y-agregarla-al-carrito",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 26,
  "name": "ingreso al menu y selecciono una \u003ccategoria\u003e de busqueda",
  "rows": [
    {
      "cells": [
        "Laptops"
      ],
      "line": 27
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 28,
  "name": "selecciono un \u003cobjeto\u003e",
  "rows": [
    {
      "cells": [
        "Dell i7 8gb"
      ],
      "line": 29
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "lo agrego al carrito",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "verifico que se agrego correctamente",
  "keyword": "And "
});
formatter.match({
  "location": "PageSteps.ingreso_al_menu_y_selecciono_una_categoria_de_busqueda(DataTable)"
});
formatter.result({
  "duration": 3374109300,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.selecciono_un_objeto(DataTable)"
});
formatter.result({
  "duration": 2435164000,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.lo_agrego_al_carrito()"
});
formatter.result({
  "duration": 4143725600,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.verifico_que_se_agrego_correctamente()"
});
formatter.result({
  "duration": 3156084200,
  "status": "passed"
});
formatter.scenario({
  "line": 33,
  "name": "Logout",
  "description": "",
  "id": "seleccionar-una-laptop-dentro-de-la-pagina-web;logout",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 34,
  "name": "ingreso al menu logout",
  "keyword": "Given "
});
formatter.step({
  "line": 35,
  "name": "me deslogueo de la cuenta",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "finalizo el driver",
  "keyword": "And "
});
formatter.match({
  "location": "PageSteps.ingreso_al_menu_logout()"
});
formatter.result({
  "duration": 5932544500,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.me_deslogueo_de_la_cuenta()"
});
formatter.result({
  "duration": 106863800,
  "status": "passed"
});
formatter.match({
  "location": "PageSteps.finalizo_el_driver()"
});
formatter.result({
  "duration": 1113779800,
  "status": "passed"
});
});